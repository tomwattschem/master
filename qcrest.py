#!/usr/bin/env python3

import os
import argparse
import operator


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", action='store', nargs='+', help='.xyz files containing molecular geometry')
    parser.add_argument("-g", help="Specify solvent to be used (GBSA) (default gas-phase)")
    parser.add_argument("-T", type=int, default=4, help="Specify number of threads to use (default 4)")
    parser.add_argument("-ewin", type=float, help="Specify energy threshold to use (default 6)")
    parser.add_argument("-mdlen", type=int, help="Specify length of MTD/MD runs (in ps)")
    parser.add_argument("-chrg", type=int, help="Specify charge (default 0)")
    parser.add_argument("-v1", action="store_true", help="Use MF/MD/GC protocol instead of metadynamics")
    return parser.parse_args()


def get_decomposition(file_names, n_cores=4):

    file_names_and_n_cores = {}
    for xyz_filename in [filename for filename in file_names if filename.endswith('.xyz')]:
            file_names_and_n_cores[xyz_filename] = n_cores

    # From the file names and the number of cores requested in each find the decomposition that affords totals
    # less than 36 cores, which will be executed exclusively on one node

    filenames_blocks_and_n_cores = []
    block_ncores = 0
    filenames_block = []

    # Sort the dictionary on the number of cores so we use *almost* the fewest number of notes
    for filename, n_cores in sorted(file_names_and_n_cores.items(), key=operator.itemgetter(1)):

        if block_ncores + n_cores > 36:         # Each node has 36 cores on Cirrus
            filenames_blocks_and_n_cores.append((filenames_block, block_ncores))
            filenames_block = []
            block_ncores = 0

        filenames_block.append(filename)
        block_ncores += n_cores

    if len(filenames_block) > 0:
        filenames_blocks_and_n_cores.append((filenames_block, block_ncores))

    return filenames_blocks_and_n_cores


def print_bash_script(filenames, n_cores, sh_filename):

    with open(sh_filename, 'w') as bash_script:
        print('#!/bin/bash --login',
              f'#PBS -N {sh_filename[:-3]}',
              f'#PBS -l select=1:ncpus={n_cores}',
              '#PBS -l walltime=96:00:00',
              '#PBS -l place=scatter:excl',
              '#PBS -A ec043',
              f'export PATH=$PATH:/lustre/home/ec043/ball4935/opt/bin/',
              'export TMPDIR=/lustre/home/ec043/ball4935/tmp',
              'cd $PBS_O_WORKDIR',
              sep='\n', file=bash_script)

        for filename in filenames:
            crest_command = ""
            if args.g:
                crest_command = crest_command + f"-g {args.g} "
            if args.T:
                crest_command = crest_command + f"-T {args.T} "
            if args.ewin:
                crest_command = crest_command + f"-ewin {args.ewin} "
            if args.mdlen:
                crest_command = crest_command + f"-len {args.mdlen} "
            if args.chrg:
                crest_command = crest_command + f"-chrg {args.chrg} "
            if args.v1:
                crest_command = crest_command + "-v1 "

            basename = filename.replace('.xyz', '')
            print(f'mkdir -p {basename} && cp {filename} {basename}/ && cd {basename} && '
                  f'crest {filename} {crest_command} > {basename}.out && cd .. &',
                  file=bash_script)

        print('wait', file=bash_script)

    return sh_filename


if __name__ == '__main__':

    args = get_args()
    names_and_cores = get_decomposition(file_names=args.filenames)
    for i, (names, cores) in enumerate(names_and_cores):
        script_filename = print_bash_script(filenames=names, n_cores=cores, sh_filename=f'batch{i}.sh')
        os.system(f'qsub {script_filename}')
